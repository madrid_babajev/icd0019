package types;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class Code {

    public static void main(String[] args) {

        int[] numbers = {1, 3, -2, 9};

    }

    public static int sum(int[] numbers) {
        int output = 0;

        for (int num : numbers) {
            output += num;
        }
        return output;
    }

    public static double average(int[] numbers) {
        int elements = numbers.length;
        float counter = 0;

        for (int num : numbers) {
            counter += num;
        }
        return counter / elements;
    }

    public static Integer minimumElement(int[] integers) {
        if (integers.length == 0) {
            return null;
        }
        Arrays.sort(integers);
        // Arrays.sort(arr, Collections.reverseOrder()); reverse oodesign.order
        return integers[0];
    }

    public static String asString(int[] elements) {
        if (elements.length == 0) {
            return "";
        }

        StringBuilder retString = new StringBuilder();
        int i = 0;
        for (int num: elements) {
            if (i != elements.length - 1) {
                retString.append(num);
                retString.append(", ");
            }
            else {
                retString.append(num);
            }
            i++;
        }

        return retString.toString();
    }

    public static Character mode(String input) {
        Map<Character,Integer> someMap = new HashMap<>();
        for (char currentChar: input.toCharArray()) {
            if (someMap.containsKey(currentChar) ) {
                int oldValue = someMap.get(currentChar);
                someMap.put(currentChar, oldValue + 1);

            }
            else {
                someMap.put(currentChar, 1);
            }
        }

        Character[] retArray = new Character[1];
        int temp = 0;
        for (int i = 0; i < 2; i++) {
            for (Map.Entry<Character, Integer> element : someMap.entrySet()) {
                if (element.getValue() > temp && i != 1) {
                    temp = element.getValue();
                }

                else if (i == 1 && temp == element.getValue()) {
                    retArray[0] = element.getKey();
                    }

            }
        }
        return retArray[0];
    }

    public static String squareDigits(String s) {
        char[] charArray = s.toCharArray();
        StringBuilder retString = new StringBuilder();

        for (char chr: charArray) {
            if (Character.isDigit(chr)) {
                int number = Character.getNumericValue(chr);
                number = number * number;
                char[] numberCharArray = String.valueOf(number).toCharArray();
                for (char chrNum: numberCharArray) {
                    retString.append(chrNum);
                }
            }

            else {
                retString.append(chr);
            }
        }

        return retString.toString();
    }
}
//
//    public static int isolatedSquareCount() {
//        boolean[][] matrix = getSampleMatrix();
//
//        printMatrix(matrix);
//
//        int isolatedCount = 0;
//
//
//        // count isolates squares here
//
//
//        return isolatedCount;
//    }
//
//    private static void printMatrix(boolean[][] matrix) {
//        for (boolean[] row : matrix) {
//            System.out.println(Arrays.toString(row));
//        }
//    }
//
//    private static boolean[][] getSampleMatrix() {
//        boolean[][] matrix = new boolean[10][10];
//
//        Random r = new Random(5);
//        for (int i = 0; i < matrix.length; i++) {
//            for (int j = 0; j < matrix[0].length; j++) {
//                matrix[i][j] = r.nextInt(5) < 2;
//            }
//        }
//
//        return matrix;
//    }
//}
