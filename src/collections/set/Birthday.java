package collections.set;

import org.junit.Test;

import java.util.*;


public class Birthday {

    @Test
    public void runCode() {

        List<Integer> countCollisions = new ArrayList<>();
        for (int i = 0; i < 1000; i++) {
            countCollisions.add(findFirstCollision());
        }

        int accumulator = 0;
        for (Integer num: countCollisions) {
            accumulator += num;
        }
        System.out.println(accumulator / countCollisions.size());
    }

    private Integer findFirstCollision() {
        Random r = new Random();

        int randomDayOfYear = r.nextInt(365);

        // pick random day in a loop
        // find how many iterations till first collision (got the same number)
        Set<Integer> set = new HashSet<>();
        int iterationTracker = 0;
        while (true) {
            int randomDay = r.nextInt(365);
            if (set.contains(randomDay)) {
                return iterationTracker;
            }
            else {
                set.add(randomDay);
            }
            iterationTracker++;

        }
    }

}
