package collections.streaks;

import java.util.*;

public class Code {

    public static List<List<String>> getStreakList(String input) {

        if (input == null || input.equals("")) {
            return new ArrayList<>();
        }

        List<List<String>> retList = new ArrayList<>();
        List<String> temp = new ArrayList<>();


        for (Character currentChar: input.toCharArray()) {
            if (temp.contains(currentChar.toString())) {
                temp.add(currentChar.toString());
            }
            else if (temp.size() == 0) {

                temp.add(currentChar.toString());
            }
            else {
                retList.add(temp);
                temp = new ArrayList<>();
                temp.add(currentChar.toString());
            }
        }
        if (temp.size() > 0) {
            retList.add(temp);
        }

        return retList;

        }


}

