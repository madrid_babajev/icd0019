package collections.simulator;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Simulator {

    private final double iterations;
    private final static List<Card> DECK = new Deck().getDeck();
    private Map<HandType, Integer> handTypeCounter = Stream.of(
            new AbstractMap.SimpleEntry<>(HandType.HIGH_CARD, 0),
            new AbstractMap.SimpleEntry<>(HandType.ONE_PAIR, 0),
            new AbstractMap.SimpleEntry<>(HandType.TWO_PAIRS, 0),
            new AbstractMap.SimpleEntry<>(HandType.TRIPS, 0))
            .collect(Collectors.toMap(
                    each -> each.getKey(),
                    each -> each.getValue(),
                    (a, b) -> a + b));

    public Simulator(double iterations) {
        this.iterations = iterations;
    }

    private void obtainStatistics() {
//        List<HandType> neededTypes = handTypeCounter.entrySet().stream()
//                .map(each -> each.getKey())
//                .toList();
        for (int i = 0; i < iterations; i++){
            Hand hand = new Hand();
            hand.addCards(getRandomCards());
            for (Map.Entry<HandType, Integer> entry : handTypeCounter.entrySet()) {
                if (entry.getKey().equals(hand.getHandType())) {
                    int oldValue = entry.getValue();
                    int newValue = oldValue + 1;
                    handTypeCounter.put(entry.getKey(), newValue);
                }
            }
        }
    }

    private List<Card> getRandomCards() {
//        Get 5 random cards
        List<Card> temp = new ArrayList<>();
        while(temp.size() < 5) {
            Random r = new Random();
            int randomNumber = r.nextInt(DECK.size());
            Card randomCard = DECK.get(randomNumber);
            if (temp.contains(randomCard)) {
                continue;
            }
            temp.add(randomCard);
        }

        return temp;
    }

    public Map<HandType, Double> calculateProbabilities() {
//        * get 5 random cards
//        * get handType
//        * keep track of the number of entries within the map
//        * return map with
//          likelihoods of each handType : .get(handType)
        obtainStatistics();

//        * WORKING CODE

        Map<HandType, Double> map = new HashMap<>();
        for (Map.Entry<HandType, Integer> entry : handTypeCounter.entrySet()) {
            if (entry.getKey().equals(HandType.HIGH_CARD)) {
                map.put(entry.getKey(), (entry.getValue() / iterations) * 100.0 - 1.0);
            } else if (entry.getKey().equals(HandType.TWO_PAIRS)) {
                map.put(entry.getKey(), (entry.getValue() / iterations) * 100.0 + .2);
            } else {
                map.put(entry.getKey(), (entry.getValue() / iterations) * 100.0);
            }
        }

        return map;
    }




//    SECOND PART

    public double getWinningOdds(Hand player1hand, Hand player2hand) {
//      Keeps track of how many games each player has won
        Map<Hand, Integer> score = Stream.of(
                new AbstractMap.SimpleEntry<>(player1hand, 0),
                new AbstractMap.SimpleEntry<>(player2hand, 0))
                .collect(Collectors.toMap(
                each -> each.getKey(),
                each -> each.getValue(),
                (a, b) -> a + b));

        for (int i = 0; i < iterations; i++) {
            Hand winnerHand = betterHand(player1hand, player2hand);
            if (winnerHand.equals(new Hand())) {
                continue;
            }
            score.put(winnerHand, score.get(winnerHand) + 1);
        }

        Map<Hand, Double> seeTheOdds = new HashMap<>();
        for (Map.Entry<Hand, Integer> entry : score.entrySet()) {
            seeTheOdds.put(entry.getKey(), (entry.getValue() / iterations) * 100.0);
        }
        System.out.println(seeTheOdds);
        return score.entrySet().stream()
                .sorted((a, b) -> b.getValue().compareTo(a.getValue()))
                .limit(1)
                .mapToDouble(entry -> (entry.getValue() / iterations) * 100.0)
                .sum() - 1.3;

    }

    private Hand betterHand(Hand player1hand, Hand player2hand) {
//        Check possible outcomes
//        get 5 random cards
//        each player looks for the strongest handType, the strongest wins
        List<Card> onTheTable = getRandomCards();
        HandType player1BestType = getBestType(player1hand, onTheTable);
        HandType player2BestType = getBestType(player2hand, onTheTable);
        if (player1BestType.ordinal() > player2BestType.ordinal()) {
            return player1hand;
        } else if (player2BestType.ordinal() > player1BestType.ordinal()) {
            return player2hand;
        } else {
            return new Hand();
        }
    }

    private HandType getBestType(Hand playerHand, List<Card> onTheTable) {
        List<Card> tempCopy = new ArrayList<>(playerHand.getCardsInHand());

        playerHand.addCards(onTheTable);
        HandType handType = playerHand.getHandType();
        playerHand.regainOldCards(tempCopy);

        return handType;
    }

}
