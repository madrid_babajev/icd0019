package collections.simulator;

import java.util.*;

public class Hand implements Iterable<Card> {

    private Map<Card.CardValue, Integer> cardsMap;
//
//    public Hand() {
//        this.cardsMap = getMap();
//    }

private List<Card> cards = new ArrayList<>();

    public void addCard(Card card) {
        cards.add(card);
    }

    public void removeCards(List<Card> cardsList) {
        cards.removeAll(cardsList);
    }

    public List<Card> getCardsInHand() {
        return cards;
    }

    public void regainOldCards(List<Card> cardList) {
        cards = cardList;
    }

    public void addCards(List<Card> cardsToAdd) {
        cards.addAll(cardsToAdd);
    }

    @Override
    public String toString() {
        return cards.toString();
    }

    public HandType getHandType() {
        cardsMap = getMap();
        if (this.checkedForFourOfAKind()) {
            cardsMap.clear();
            return HandType.FOUR_OF_A_KIND;
        }
        else if (checkedForFullHouse()) {
            cardsMap.clear();
            return HandType.FULL_HOUSE;
        }
        else if (checkedForFlush() && checkedForStraight()) {
            cardsMap.clear();
            return HandType.STRAIGHT_FLUSH;
        }
        else if (checkedForFlush()) {
            cardsMap.clear();
            return HandType.FLUSH;
        }
        else if (checkedForStraight()) {
            cardsMap.clear();
            return HandType.STRAIGHT;
        }
        else if (checkedForTrips()) {
            cardsMap.clear();
            return HandType.TRIPS;
        }
        else if (checkedForTwoPairs()) {
            cardsMap.clear();
            return HandType.TWO_PAIRS;
        }
        else if (checkedForOnePair()) {
            cardsMap.clear();
            return HandType.ONE_PAIR;
        }
        else {
            cardsMap.clear();
            return HandType.HIGH_CARD;
        }

    }

    private boolean checkedForFourOfAKind() {
        Map<Card.CardValue, Integer> map = cardsMap;

        for (Map.Entry<Card.CardValue, Integer> item: map.entrySet()) {
            if (item.getValue() >= 4) {
                return true;
            }
        }
        return false;
    }

    private HashMap<Card.CardValue, Integer> getMap() {
        HashMap<Card.CardValue, Integer> map = new HashMap<>();

        for (Card currentCard: cards) {
            if (!map.containsKey(currentCard.getValue())) {
                map.put(currentCard.getValue(), 1);
            }
            else {
                int updateValue = map.get(currentCard.getValue()) + 1;
                map.put(currentCard.getValue(), updateValue);
            }
        }
        return map;
    }

    private List<Card.CardValue> getCardValueList() {
        List<Card.CardValue> retList = new ArrayList<>();
        for (Card currentCard: cards) {
            retList.add(currentCard.getValue());
        }
        return retList;
    }

    private boolean checkedForFullHouse() {
        Map<Card.CardValue, Integer> map = cardsMap;

        boolean triple = false;
        boolean pair = false;
        for (Map.Entry<Card.CardValue, Integer> item: map.entrySet()) {
            if (item.getValue() == 3) {
                triple = true;
            }
            else if (item.getValue() == 2) {
                pair = true;
            }
        }
        return triple && pair;
    }

    private boolean checkedForFlush() {
        Map<Card.CardSuit, Integer> map = new HashMap<>();

        for (Card currentCard: cards) {
            if (!map.containsKey(currentCard.getSuit())) {
                map.put(currentCard.getSuit(), 1);
            }
            else {
                int updatedValue = map.get(currentCard.getSuit()) + 1;
                map.put(currentCard.getSuit(), updatedValue);
            }
        }
        for (Map.Entry<Card.CardSuit, Integer> item: map.entrySet()) {
            if (item.getValue() == 5) {
                return true;
            }
        }
        return false;
    }

    private boolean checkedForStraight() {
        if (cards.size() < 5) {
            return false;
        }
        Collections.sort(cards);
        List<Card.CardValue> cardValuesGeneral = Arrays.stream(Card.CardValue.values()).
                toList();
        List<Card.CardValue> cardValuesHand = getCardValueList();

        List<Integer> numSequence = new ArrayList<>();
        int index = -1;
        for (Card.CardValue currentCardGeneral: cardValuesGeneral) {
            index++;
            for (Card.CardValue currentCardHand: cardValuesHand) {
                if (currentCardGeneral.equals(currentCardHand)) {
                    numSequence.add(index);
                }
            }
        }

        int streak = 0;
        int firstNum = numSequence.get(0);

        for (Integer num: numSequence) {
            if (num == firstNum) {
                streak++;
                continue;
            }
            if (num == 12 && streak == 4 || streak == 5) {
                return true;
            }

            if (num == firstNum + 1) {
                streak++;
                firstNum = num;
            }
            else {
                break;
            }
        }
        return streak == 5;
    }

    private boolean checkedForTrips() {
        Map<Card.CardValue, Integer> map = cardsMap;

        for (Map.Entry<Card.CardValue, Integer> item: map.entrySet()) {
            if (item.getValue() == 3) {
                return true;
            }
        }
        return false;

    }

    private boolean checkedForTwoPairs() {
        Map<Card.CardValue, Integer> map = cardsMap;

        boolean firstPairFound = false;
        boolean secondPairFound = false;

        for (Map.Entry<Card.CardValue, Integer> item: map.entrySet()) {

            if (!firstPairFound && item.getValue() == 2) {
                firstPairFound = true;
            }
            else if (firstPairFound && item.getValue() == 2) {
                secondPairFound = true;
            }
        }
        return secondPairFound;
    }

    private boolean checkedForOnePair() {
        Map<Card.CardValue, Integer> map = cardsMap;

        for (Map.Entry<Card.CardValue, Integer> item: map.entrySet()) {
            if (item.getValue() == 2) {
                return true;
            }
        }
        return false;
    }

    public boolean contains(Card card) {
        return cards.contains(card);
    }

    public boolean isEmpty() {
        return cards.isEmpty();
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Hand)) {
            return false;
        }
        return ((Hand) other).getCardsInHand().isEmpty()
                && this.getCardsInHand().isEmpty();
    }

    @Override
    public Iterator<Card> iterator() {
        return cards.iterator();
    }
}
