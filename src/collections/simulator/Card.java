package collections.simulator;

import java.util.Arrays;
import java.util.Objects;

public class Card implements Comparable<Card> {

//    private static Card.CardValue cardValue;

    public enum CardValue { S2, S3, S4, S5, S6, S7, S8, S9, S10, J, Q, K, A }

    public enum CardSuit { C, D, H, S }

    private final CardValue value;
    private final CardSuit suit;


    public Card(CardValue value, CardSuit suit) {
        this.value = value;
        this.suit = suit;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Card other)) {
            return false;
        }

        return Objects.equals(value, other.value)
                && Objects.equals(suit, other.suit);
    }

    @Override
    public int compareTo(Card other) {

        // Get indexes cards' value
        int otherIndex = Arrays.stream(Card.CardValue.values())
                .toList().indexOf(other.getValue());
        int thisIndex = Arrays.stream(Card.CardValue.values())
                .toList().indexOf(this.getValue());

        if (thisIndex == otherIndex) {
            return 0;
        }
        else if (thisIndex > otherIndex) {
            return 1;
        }
        else {
            return -1;
        }
    }

    public CardValue getValue() {
        return value;
    }

    public CardSuit getSuit() {
        return suit;
    }

    @Override
    public String toString() {
        return "(%s, %s)".formatted(value, suit);
    }
}
