package collections.simulator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Deck {

    private List<Card> deck = new ArrayList<>();

    public Deck() {
        List<Card.CardValue> values = Arrays.stream(Card.CardValue
                .values()).toList();
        List<Card.CardSuit> suits = Arrays.stream(Card.CardSuit
                .values()).toList();

        for (Card.CardValue value : values) {
            for (Card.CardSuit suit : suits) {
                deck.add(new Card(value, suit));
            }
        }
    }

    public List<Card> getDeck() {
        return deck;
    }
}
