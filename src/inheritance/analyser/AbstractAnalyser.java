package inheritance.analyser;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public abstract sealed class AbstractAnalyser permits DifferentiatedTaxSalesAnalyser, FlatTaxSalesAnalyser, TaxFreeSalesAnalyser {

    protected List<SalesRecord> records;

    public AbstractAnalyser(List<SalesRecord> records) {
        this.records = records;
    }

    public Double getTotalSales() {
        Double totalValue = 0.0;
        for (SalesRecord record : records) {
            totalValue += accountForTaxes(record);
        }
        return totalValue;
    }

    protected abstract Double accountForTaxes(SalesRecord record);

    public Double getTotalSalesByProductId(String id) {

        return records.stream()
                .filter(record -> record.getProductId().equals(id))
                .mapToDouble(record -> accountForTaxes(record))
                .sum();
    }

    public String getIdOfMostPopularItem() {
        Map<String, Integer> map = records.stream()
                .collect(Collectors.toMap(
                        each -> each.getProductId(),
                        each -> each.getItemsSold(),
                        (a, b) -> a + b));

        return map.entrySet().stream()
                .sorted((a, b) -> b.getValue().compareTo(a.getValue()))
                .limit(1)
                .map(entry -> entry.getKey())
                .collect(Collectors.joining(""));
    }

    protected String getIdOfItemWithLargestTotalSales() {

        return records.stream()
                .collect(Collectors.toMap(
                        each -> each.getProductId(),
                        each -> accountForTaxes(each),
                        (a, b) -> a + b))
                .entrySet().stream()
                .sorted((a, b) -> b.getValue().compareTo(a.getValue()))
                .limit(1)
                .map(record -> record.getKey())
                .collect(Collectors.joining(""));
    }
}
