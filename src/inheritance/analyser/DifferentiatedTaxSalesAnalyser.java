package inheritance.analyser;

import java.util.List;

public final class DifferentiatedTaxSalesAnalyser extends AbstractAnalyser {

    private final static Double TAX_RATE = 20.0;
    private final static Double REDUCED_TAX_RATE = 10.0;

    public DifferentiatedTaxSalesAnalyser(List<SalesRecord> records) {
        super(records);
    }

    @Override
    protected Double accountForTaxes(SalesRecord record) {
        Double tax = TAX_RATE;
        if (record.hasReducedRate()) {
            tax = REDUCED_TAX_RATE;
        }
        return (record.getProductPrice() * record.getItemsSold())
                * 100 / (100 + tax);
    }

}
