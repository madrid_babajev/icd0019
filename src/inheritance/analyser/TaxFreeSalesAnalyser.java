package inheritance.analyser;

import java.util.List;

public final class TaxFreeSalesAnalyser extends AbstractAnalyser {

    private final static Double TAX_RATE = 0.0;

    public TaxFreeSalesAnalyser(List<SalesRecord> records) {
        super(records);
    }

    @Override
    protected Double accountForTaxes(SalesRecord record) {
        return (record.getProductPrice() * record.getItemsSold())
                * 100 / (100 + TAX_RATE);
    }

}
