package inheritance.pager;

import java.util.ArrayList;
import java.util.List;

public class FilteringPager {

    private final SimplePager dataSource;

    private final int pageSize;

    private int pageNumber = -1;

    private List<Integer> currentPage;

    private List<Integer> combined;
    private boolean successful = false;

    public FilteringPager(SimplePager dataSource, int pageSize) {
        this.dataSource = dataSource;
        this.pageSize = pageSize;
        this.currentPage = new ArrayList<>();
        this.combined = new ArrayList<>();
    }

    public List<Integer> getNextPage() {
//            1, null, null, null, 2, 3, 4  :  4
//            -> tempList = [1]  combined = [1]
//            -> tempList = [2, 3, 4]  combined = [1, 2]
        try {

            boolean firstTry = true;
            combined = new ArrayList<>();
            while (!successful) {
                List<Integer> tempList = filterTempList(
                        dataSource.getPage(++pageNumber));
                obtainCombined(tempList, firstTry);
                firstTry = false;
            }
            successful = false;
            return combined;

        } catch (IllegalArgumentException e) {
            throw new IllegalStateException();
        }
    }

    private List<Integer> filterTempList(List<Integer> tempList) {
        return tempList.stream()
                .filter(num -> num != null)
                .toList();
    }

    public List<Integer> getCurrentPage() {
        if (currentPage.isEmpty()) {
            throw new IllegalStateException();
        }
        System.out.println(currentPage);
        return currentPage;
    }

    public List<Integer> getPreviousPage() {
        try {

            boolean firstTry = true;
            combined = new ArrayList<>();
            while (!successful) {
                List<Integer> tempList = filterTempList(
                        dataSource.getPage(--pageNumber));
                obtainCombined(tempList, firstTry);
                firstTry = false;
            }
//            List<Integer> copyOfCombined = new ArrayList<>();
//            copyOfCombined.addAll(combined);
//            combined.clear();
            successful = false;
            return combined;

        } catch (IllegalArgumentException e) {
            throw new IllegalStateException();
        }
    }

    private void obtainCombined(List<Integer> tempList, boolean firstTry) {
        if (tempList.size() == pageSize && firstTry) {
            combined = tempList;
            updateCurrentPage();
            successful = true;
            return;
        }
        if (combined.isEmpty()) {
            combined.addAll(tempList);
        } else {
            if (combined.size() + tempList.size() < pageSize) {
                combined.addAll(tempList);
            } else {
                for (int i = 0; i < tempList.size(); i++) {
                    combined.add(tempList.get(0));
                    if (combined.size() == pageSize) {
                        updateCurrentPage();
                        successful = true;
                        break;
                    }
                }
            }
        }
    }

    private void updateCurrentPage() {
        currentPage = new ArrayList<>();
        for (Integer num : combined) {
            currentPage.add(num);
        }
    }
}
