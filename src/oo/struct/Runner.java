package oo.struct;

import org.junit.Test;
import oo.struct.Point3D;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.*;

public class Runner {

    @Test
    public void coordinatesAsArrays() {

        int[][] trianglePoints = {{1, 1, 0}, {5, 1, 0}, {3, 7, 1}};

        for (int[] each : trianglePoints) {
            System.out.println(each[2]);
        }

    }

    @Test
    public void coordinatesAsObjects() {
        int[][] trianglePoints = {{1, 1, 0}, {5, 1, 0}, {3, 7, 1}};
        Point3D[] objectsArray = new Point3D[3];
        int index = 0;

        for (int[] item: trianglePoints) {
            Point3D newObject = new Point3D(item[0], item[1], item[2]);
            objectsArray[index] = newObject;
            index++;
        }

        assertThat(objectsArray[0].z, is(0));
        assertThat(objectsArray[1].z, is(0));
        assertThat(objectsArray[2].z, is(1));
    }
}
