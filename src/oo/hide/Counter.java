package oo.hide;

public class Counter {

    public int start;
    public int step;
    public boolean firstCycle;

    public Counter(int start, int step) {
        this.start = start;
        this.step = step;
        this.firstCycle = true;
    }

    public int nextValue() {
        if (firstCycle) {
            firstCycle = false;
            return start;
        }
        int retValue = start + step;
        start = retValue;

        return retValue;
    }
}
