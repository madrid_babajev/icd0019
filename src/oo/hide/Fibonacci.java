package oo.hide;

public class Fibonacci {
    private int last;
    private int secondLast;
    private boolean firstCycle;

    public Fibonacci() {
        this.last = 1;
        this.secondLast = 0;
        this.firstCycle = true;
    }

    public int nextValue() {
        if (firstCycle) {
            firstCycle = false;
            return 0;
        }

        int another = last + secondLast;
        secondLast = last;
        last = another;

        return secondLast;
    }

}
