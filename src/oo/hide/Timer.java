package oo.hide;

public class Timer {

    private long start = System.currentTimeMillis();

    public String getPassedTime() {
        return String.valueOf(System.currentTimeMillis() - start);
    }
}
