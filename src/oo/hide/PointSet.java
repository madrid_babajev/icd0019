package oo.hide;

//import java.util.Arrays;

public class PointSet {
    private Point[] pointArray;
    private int index;


    public PointSet(int capacity) {
        this.pointArray = new Point[capacity];
        index = 0;
    }

    public PointSet() {
        this(10);
    }

    public void add(Point point) {
        boolean duplicateFound = false;

        for (Point currentPoint: pointArray) {
            if (point.equals(currentPoint)) {
                duplicateFound = true;
                break;
            }
        }
        if (!duplicateFound) {
            try {
                pointArray[index] = point;
                indexTracker();
            }
            catch (ArrayIndexOutOfBoundsException e) {
                int newCapacity = this.size() * 2;
                PointSet tempObject = new PointSet(newCapacity);
                for (Point currentPoint: pointArray) {
                    tempObject.add(currentPoint);
                }
                this.pointArray = tempObject.pointArray;
                pointArray[index] = point;
                indexTracker();
            }
        }
    }

    private void indexTracker() {
        index++;
    }

    public int size() {
        int counter = 0;

        for (Point currentPoint: pointArray) {
            if (currentPoint != null) {
                counter++;
            }
        }

        return counter;
    }

    public boolean contains(Point point) {
        for (Point currentPoint: pointArray) {
            if (currentPoint != null && currentPoint.equals(point)) {
                return true;
            }
        }

        return false;
    }

    public PointSet subtract(PointSet other) {

        PointSet retObject = new PointSet();

        for (Point currentPoint: pointArray) {
            if (currentPoint != null && !(other.contains(currentPoint))) {
                retObject.add(currentPoint);
            }
        }

        return retObject;
    }

    public PointSet intersect(PointSet other) {
        PointSet retObject = new PointSet();

        for (Point currentPoint: pointArray) {
            if (currentPoint != null && other.contains(currentPoint)) {
                retObject.add(currentPoint);
            }
        }

        return retObject;
    }

    @Override
    public String toString() {
        StringBuilder retString = new StringBuilder();
        int index = 0;
        for (Point currentPoint: pointArray) {
            if (currentPoint != null) {
                if (index != this.size() - 1) {
                    retString.append(currentPoint);
                    retString.append(", ");
                    index++;
                }
                else {
                    retString.append(currentPoint);
                }
            }
        }
        return retString.toString();
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof PointSet) ) {
            return false;
        }
//        if (!(this.size() == ((PointSet) other).size())) {
//            return false;
//        }

        for (Point currentPoint: pointArray) {
            boolean containsCheck = ((PointSet) other).contains(currentPoint);
            if (!containsCheck && currentPoint != null) {
                return false;
            }
        }

        return true;
    }
}
