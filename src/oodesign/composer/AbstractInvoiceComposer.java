package oodesign.composer;

import oodesign.order.Order;
import oodesign.order.OrderRow;

public abstract class AbstractInvoiceComposer {

    protected Order order;

    public AbstractInvoiceComposer(Order order) {
        this.order = order;
    }

    public abstract String getTitle();

    public abstract String getBody();

    protected Integer getOrderTotal(Order order) {
        int total = 0;
        for (OrderRow orderRow : order.getOrderRows()) {
            total += orderRow.getPrice() * orderRow.getQuantity();
        }
        return total;
    }

}
