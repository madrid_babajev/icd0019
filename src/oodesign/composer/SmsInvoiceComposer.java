package oodesign.composer;

import oodesign.order.Order;
import oodesign.order.OrderRow;

public class SmsInvoiceComposer extends AbstractInvoiceComposer {

    public SmsInvoiceComposer(Order order) {
        super(order);
    }

    @Override
    public String getTitle() {
        return "Teavitus arvest";
    }

    @Override
    public String getBody() {
        return "Teile on koostatud arve summas: " + getOrderTotal(order) + " eurot";
    }

}
