package oodesign.composer;

import oodesign.order.Order;
import oodesign.order.OrderRow;

public class MainForComposer {

    public static void main(String[] args) {

        Order order = new Order("A123");
        order.add(new OrderRow("CPU", 2, 100));
        order.add(new OrderRow("Motherboard", 3, 60));

        AbstractInvoiceComposer composer = new EmailInvoiceComposer(order);


        System.out.println(composer.getTitle());
        System.out.println(composer.getBody());
//        System.out.println("");
//        System.out.println(composer1.getTitle());
//        System.out.println(composer1.getBody());
    }

}
