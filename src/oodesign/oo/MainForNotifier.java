package oodesign.oo;

import oodesign.message.EmailInvoiceSender;
import oodesign.message.SmsInvoiceSender;
import oodesign.order.Order;
import oodesign.order.OrderRow;

public class MainForNotifier {

    public static void main(String[] args) {

        Order order = new Order("A123");
        order.setChannel("email");
        order.add(new OrderRow("CPU", 2, 100));
        order.add(new OrderRow("Motherboard", 3, 60));

        OrderNotifier notifier = new OrderNotifier();

        notifier.registerChannelNotifier("sms", new SmsInvoiceSender());
        notifier.registerChannelNotifier("email", new EmailInvoiceSender());

        notifier.notifyAbout(order);
    }

}
