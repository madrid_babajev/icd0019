package oodesign.message;

import oodesign.order.Order;
import oodesign.order.OrderRow;

public class MainForMessageSender {

    public static void main(String[] args) {

        Order order = new Order("A123");
        order.add(new OrderRow("CPU", 2, 100));
        order.add(new OrderRow("Motherboard", 3, 60));

        EmailInvoiceSender sender = new EmailInvoiceSender();

        sender.sendInvoiceMessage(order);
    }

}
