package oodesign.message;

import oodesign.composer.EmailInvoiceComposer;
import oodesign.monolith.MonolithNotifier;
import oodesign.order.Order;

public class EmailInvoiceSender implements InvoiceSender {

    @Override
    public void sendInvoiceMessage(Order order) {
        EmailInvoiceComposer composer = new EmailInvoiceComposer(order);

        EmailSender sender = new EmailSender(composer.getTitle(), composer.getBody());

        sender.send();
    }

    private static class EmailSender {
        private String title;
        private String body;

        public EmailSender(String title, String body) {
            this.title = title;
            this.body = body;
        }

        public void send() {
            System.out.println("Sending email ...");
            System.out.println("Title: " + title);
            System.out.println("Body: " + body);
        }
    }
}

