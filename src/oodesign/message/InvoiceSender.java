package oodesign.message;

import oodesign.order.Order;

public interface InvoiceSender {
    void sendInvoiceMessage(Order order);
}
