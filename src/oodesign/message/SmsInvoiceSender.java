package oodesign.message;

import oodesign.composer.SmsInvoiceComposer;
import oodesign.order.Order;

public class SmsInvoiceSender implements InvoiceSender {

    @Override
    public void sendInvoiceMessage(Order order) {
        SmsInvoiceComposer composer = new SmsInvoiceComposer(order);

        sendSms(composer.getTitle(), composer.getBody());
    }

    private void sendSms(String title, String contents) {
        System.out.println("Sending sms ...");
        System.out.println("Title: " + title);
        System.out.println("Body: " + contents);
    }
}
