package intro;

public class Program {

    public static void main(String[] args) {

        int decimal = asDecimal("11001101");

        System.out.println(decimal); // 205
        System.out.println(asString(205));
    }

    public static String asString(int input) {
        String ret_string = "";
        while (input > 0) {
            if (input % 2 == 0) {
                ret_string = "0" + ret_string;

            }
            else {
                ret_string = "1" + ret_string;

            }
            input = input / 2;
        }
        return ret_string;
    }

    public static int asDecimal(String input) {
        StringBuilder reversed = new StringBuilder(input).reverse();
        String some_string = reversed.toString();
        char[] chars = some_string.toCharArray();
        int counter = 0;
        int count_i = 1;
        for (char num: chars) {
            int current = Character.getNumericValue(num);
            counter = counter + (current * count_i);
            count_i = count_i * 2;
        }

        return counter;

    }

//    private static int pow(int arg, int power) {
//        // Java has Math.pow() but this time write your own implementation.
//
//        return 0  ;
//    }
}
