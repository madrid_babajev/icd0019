package gol;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class Game {

    private List<Point> aliveCells = new ArrayList<>();

    public void markAlive(int x, int y) {
        if (!isAlive(x, y)) {
            aliveCells.add(new Point(x, y));
        }
    }

    public boolean isAlive(int x, int y) {
        Point cellToCheck = new Point(x, y);
        return aliveCells.stream().anyMatch(currentCell -> currentCell.equals(cellToCheck));
    }

    public void eradicate(int x, int y) {
        Point cellToCheck = new Point(x, y);
        int index = -1;
        for (int i = 0; i < aliveCells.size(); i++) {
            Point currentCell = aliveCells.get(i);
            if (currentCell.equals(cellToCheck)) {
                index = i;
                break;
            }
        }
        if (index != -1) {
            aliveCells.remove(index);
        }
    }

    public void toggle(int x, int y) {
        if (!isAlive(x, y)) {
            markAlive(x, y);
        }
        else {
            eradicate(x, y);
        }
    }

    public Integer getNeighbourCount(int x, int y) {
        /*  Enumerate the cases:

        * x + 1 ; y
        * x - 1 ; y
        * x + 1 ; y + 1
        * x + 1 ; y - 1
        * x - 1 ; y + 1
        * x - 1 ; y - 1
        * x     ; y + 1
        * x     ; y - 1

        Point(0, 0)
        Point(0, 1)
        Point(0, 2)
        Point(1, 1) -> 4
        Point(2, 2)
        * */

        int counter = 0;
        for (int xCoordinate = x - 1; xCoordinate <= x + 1; xCoordinate++) {
            for (int yCoordinate = y + 1; yCoordinate >= y - 1; yCoordinate--) {
                if (xCoordinate == x && yCoordinate == y) {
                    continue;
                }
                if (isAlive(xCoordinate, yCoordinate)) {
                    counter++;
                }
            }
        }

        return counter;
    }

    public void nextFrame() {

        List<Point> tempList = new ArrayList<>();
        for (Point cell: aliveCells) {

            for (int xCoordinate = cell.x - 1; xCoordinate <= cell.x + 1; xCoordinate++) {

//                Uncomment this for faster tests.
//                if (xCoordinate < 0) {
//                    continue;
//                }
                for (int yCoordinate = cell.y + 1; yCoordinate >= cell.y - 1; yCoordinate--) {
//                    if (yCoordinate < 0) {
//                        continue;
//                    }
                    boolean currentState = isAlive(xCoordinate, yCoordinate);
                    int neighbours = getNeighbourCount(xCoordinate, yCoordinate);
                    if (nextState(currentState, neighbours)) {
                        tempList.add(new Point(xCoordinate, yCoordinate));
                    }
                }
            }
        }
        // aliveCells = tempList

        updateCells(new ArrayList<>(
                new HashSet<>(tempList)));
    }

    public void clear() {
        aliveCells.clear();
    }

    private void updateCells(List<Point> cellsUpdated) {
        clear();
        aliveCells.addAll(cellsUpdated);
    }

    public boolean nextState(boolean isLiving, int neighborCount) {
        if (isLiving) {
            return neighborCount == 2 || neighborCount == 3;
        }
        else {
            return neighborCount == 3;
        }
    }
}
