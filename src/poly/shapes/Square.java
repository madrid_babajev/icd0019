package poly.shapes;

public class Square implements Shape {
    private int side;

    public Square(int side) {
        this.side = side;
    }

    public Integer getSide() {
        return side;
    }

    public Double getArea() {
        return getSide().doubleValue() * getSide().doubleValue();
    }
}
