package poly.shapes;

public class Rectangle implements Shape {
    private int height;
    private int width;

    public Rectangle(int height, int width) {
        this.height = height;
        this.width = width;
    }

    public Integer getHeight() {
        return height;
    }

    public Integer getWidth() {
        return width;
    }

    @Override
    public Double getArea() {
        return getHeight().doubleValue() * getWidth().doubleValue();
    }
}
