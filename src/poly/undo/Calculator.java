package poly.undo;

import generics.stack.Stack;

public class Calculator {

    private Double value;
    private Stack<Double> numStack = new Stack<>();

    public void input(Number value) {
        numStack.push(0.0);
        this.value = value.doubleValue();
    }

    public void add(Number addend) {
        numStack.push(this.value);
        value += addend.doubleValue();
    }

    public void multiply(Number multiplier) {
        numStack.push(this.value);
        value *= multiplier.doubleValue();
    }

    public double getResult() {
        return value;
    }

    public void undo() {
//
//        System.out.println(numStack.pop());
//        System.out.println(numStack.pop());
//        System.out.println(numStack.pop());
        value = numStack.pop();
    }
}
