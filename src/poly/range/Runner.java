package poly.range;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class Runner {

    @Test
    public void canIterateRange() {
        for (Integer integer : range(1, 7)) {
            System.out.println(integer);
        }
    }

    private Iterable<Integer> range(int start, int end) {
//        List<Integer> retList = new ArrayList<>();
//        for (int i = start; i < end; i++) {
//            retList.add(i);
//        }
        return () -> new MyIterator(start, end);
    }

}
