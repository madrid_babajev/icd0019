package poly.fruit;

import org.junit.Test;

public class Apple implements Weighable {

    private Double weightInKiloGrams;

    public Apple(Double weight) {
        this.weightInKiloGrams = weight;
    }

    @Override
    public Integer getWeightInGrams() {
        Double temp = weightInKiloGrams * 1000;
        return temp.intValue();
    }
}

