package poly.customer;

import java.time.LocalDate;
import java.util.Objects;

public final class GoldCustomer extends AbstractCustomer {

    private LocalDate lastOrderDate;
    private final static Double GB = 1.5; /* Gold Customer Bonus */

    public GoldCustomer(String id, String name,
                        int bonusPoints, LocalDate lastOrderDate) {

        super(id, name, bonusPoints);

        this.lastOrderDate = lastOrderDate;
    }

    public GoldCustomer(String id, String name,
                        int bonusPoints) {

        super(id, name, bonusPoints);

    }

    public LocalDate getLastOrderDate() {
        return lastOrderDate;
    }

    @Override
    public void collectBonusPointsFrom(Order order) {
        if (order.getTotal() >= 100) {
            bonusPoints += order.getTotal() * GB;
        }
        lastOrderDate = order.getDate();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }

        return Objects.equals(getId(), ((GoldCustomer) obj).getId())
                && Objects.equals(getName(), ((GoldCustomer) obj).getName())
                && Objects.equals(getBonusPoints(), ((GoldCustomer) obj).getBonusPoints())
                && Objects.equals(getLastOrderDate(), ((GoldCustomer) obj).getLastOrderDate());
    }

    @Override
    public String asString() {
        return "GOLD" + ";"
                + getId() + ";"
                + getName() + ";"
                + getBonusPoints().toString() + "\n";
    }

}