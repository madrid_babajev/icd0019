package poly.customer;

import java.time.LocalDate;
import java.util.Objects;

public final class RegularCustomer extends AbstractCustomer {

    private LocalDate lastOrderDate;

    public RegularCustomer(String id, String name,
                           int bonusPoints, LocalDate lastOrderDate) {

        super(id, name, bonusPoints);

        this.lastOrderDate = lastOrderDate;
    }

    public RegularCustomer(String id, String name,
                        int bonusPoints) {

        super(id, name, bonusPoints);

    }

    public LocalDate getLastOrderDate() {
        return lastOrderDate;
    }

    @Override
    public void collectBonusPointsFrom(Order order) {
        if (lessThanMonthAgo(order.getDate())) {
            if (order.getTotal() >= 100) {
                bonusPoints += 1.5 * order.getTotal();
            }
        }
        else {
            if (order.getTotal() >= 100) {
                bonusPoints += order.getTotal();
            }
        }
        lastOrderDate = order.getDate();
    }

    private boolean lessThanMonthAgo(LocalDate orderDate) {
        if (lastOrderDate == null) {
            return false;
        }

        Integer yearsCompared = lastOrderDate.getYear() - orderDate.getYear();
        if (yearsCompared == 0) {
            Integer monthsCompared = orderDate.getMonthValue() - lastOrderDate.getMonthValue();
            if (monthsCompared <= 1) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }

        return Objects.equals(getId(), ((RegularCustomer) obj).getId())
                && Objects.equals(getName(), ((RegularCustomer) obj).getName())
                && Objects.equals(getBonusPoints(), ((RegularCustomer) obj).getBonusPoints())
                && Objects.equals(getLastOrderDate(), ((RegularCustomer) obj).getLastOrderDate());
    }

    @Override
    public String asString() {
        return "REGULAR" + ";"
                + getId() + ";"
                + getName() + ";"
                + getBonusPoints().toString() + ";"
                + getLastOrderDate() + "\n";
    }

}