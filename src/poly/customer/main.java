package poly.customer;

import java.time.LocalDate;

public class main {

    private static final BonusCollector collector = new BonusCollector(new CustomerRepository());

        public static void main(String args[]) {
//        Integer pointsBefore = getPointsFor("c3");

        collector.gatherCustomerBonus("c3", new Order(200, currentDate()));
        System.out.println();
    }

    private static Integer getPointsFor(String id) {
        return collector.getRepository().getCustomerById(id)
                .map(AbstractCustomer::getBonusPoints)
                .orElseThrow(() -> new IllegalArgumentException("unknown id: " + id));
    }

    private static LocalDate currentDate() {
        return LocalDate.parse("2022-03-28");
    }
}
