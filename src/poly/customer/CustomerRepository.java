package poly.customer;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDate;
import java.util.*;
import static java.util.stream.Collectors.toList;

public class CustomerRepository {

    private static final String FILE_PATH = "src/poly/customer/data.txt";

    private List<AbstractCustomer> customers = new ArrayList<>();

    public CustomerRepository() {
        customers.addAll(listFromData());
    }

    public List<AbstractCustomer> listFromData() {
        try {
            List<String> data = Files.readAllLines(Path.of(FILE_PATH));

            return data.stream()
                    .map(line -> Arrays.asList(line.split(";")))
                    .map(line -> {
                        if (line.get(0).equals("GOLD")) {

                            return line.size() == 5 ?
                                    new GoldCustomer(line.get(1), line.get(2),
                                            Integer.parseInt(line.get(3)),
                                            LocalDate.parse(line.get(4))) :
                                    new GoldCustomer(line.get(1), line.get(2),
                                            Integer.parseInt(line.get(3)));

                        }
                        else {
                            return line.size() == 5 ?
                                    new RegularCustomer(line.get(1), line.get(2),
                                            Integer.parseInt(line.get(3)),
                                            LocalDate.parse(line.get(4))) :
                                    new RegularCustomer(line.get(1), line.get(2),
                                            Integer.parseInt(line.get(3)));
                        }
                    }).toList();
        }
        catch (IOException e) {
            System.out.println(e);
            return List.of();
        }
    }


    public Optional<AbstractCustomer> getCustomerById(String id) {
        if (customers.isEmpty()) {
            return Optional.ofNullable(null);
        }
        List<AbstractCustomer> data = customers;
        List<AbstractCustomer> neededCustomer = data.stream()
                .filter(customer -> customer.getId().equals(id))
                .toList();
        if (neededCustomer.isEmpty()) {
            return Optional.empty();
        }
        return Optional.of(neededCustomer.get(0));
    }

    public void remove(String id) {
//        if (getCustomerById(id).isPresent()) {
//            customers.stream()
//                    .filter(currentCustomer -> currentCustomer.getId().
//                            equals(getCustomerById(id).get().getId()));
//        }

        int index = 0;
        for (int i = 0; i < customers.size(); i++) {
            if (customers.get(i).getId().equals(id)) {
                index = i;
                break;
            }
        }
        customers.remove(index);
    }

    public void save(AbstractCustomer customer) {
        boolean duplicateFound = false;
        for (AbstractCustomer currentCustomer: customers) {
            if (currentCustomer.equals(customer)) {
                duplicateFound = true;
                break;
            }
        }
        if (!duplicateFound) {
            customers.add(customer);
        }
        saveDataInAFile();

    }

    private void saveDataInAFile() {
        try {
            FileWriter file = new FileWriter(FILE_PATH);
            BufferedWriter writer = new BufferedWriter(file);
//            writer.flush();

            for (AbstractCustomer writeCustomer: customers) {
                writer.write(writeCustomer.asString());

            }
            writer.close();
        }
        catch (IOException e) {
            System.out.println(e);
        }
    }

    public int getCustomerCount() {
        return customers.size();
    }

    @Override
    public String toString() {
        return customers.toString();
    }
}
