package poly.installments;

import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;

import static java.time.temporal.TemporalAdjusters.firstDayOfMonth;

public class InstallmentGenerator {

    private LocalDate shiftingDate;
    private boolean firstShift = true;

    public List<Installment> generateRowsFor(
            Integer amount, LocalDate periodStart, LocalDate periodEnd) {

        List<Installment> retList = new ArrayList<>();

        if (amount <= 3) {
            retList.add(new Installment(amount, periodStart));
            return retList;
        }

        shiftingDate = periodStart;
        int monthsBetween = Period.between(periodStart, periodEnd).getMonths() + 1;
        List<Integer> paidPerMonthList = paidPerMonth(monthsBetween, amount);

        int cycles = 0;
        while (Period.between(shiftingDate, periodEnd).getMonths() != 0
                && paidPerMonthList.size() - 1 >= cycles) {
            if (cycles == 0) {
                retList.add(new Installment(paidPerMonthList.get(cycles), shiftingDate));
                cycles++;
                continue;
            }
            changeShiftingDate();
            retList.add(new Installment(paidPerMonthList.get(cycles), shiftingDate));
            cycles++;
        }

        return retList;
    }

    private void changeShiftingDate() {
        if (firstShift) {
            shiftingDate = shiftingDate.with(firstDayOfMonth());
            firstShift = false;
        }
        shiftingDate = shiftingDate.plusMonths(1);
    }

    private List<Integer> paidPerMonth(int months, int amount) {

        List<Integer> retList = new ArrayList<>();
        int leftOver = amount % months;
        int numberOfIntervals = months;
        int eachIntervalValue = amount / months;
        if (eachIntervalValue < 3) {
            leftOver = eachIntervalValue;
            numberOfIntervals--;
        }
        List<Integer> spreadBetweenIndexes = leftOver == 0 ?
                null : cheekyIndexes(leftOver, numberOfIntervals);

        for (int i = 0; i < numberOfIntervals; i++) {
            if (leftOver == 0) {
                retList.add(eachIntervalValue);
                continue;
            }
            if (i != spreadBetweenIndexes.get(0)) {
                retList.add(eachIntervalValue);
            }
            else {
                retList.add(eachIntervalValue + 1);
                spreadBetweenIndexes.remove(0);
            }
        }

        return retList;
    }

    private List<Integer> cheekyIndexes(int leftOver, int numOfIntervals) {

        List<Integer> retIndexes = new ArrayList<>();
        for (int i = leftOver - 1; i >= 0; i--) {
            int index = numOfIntervals - i - 1;
            retIndexes.add(index);
        }
        return retIndexes;
        }

}
