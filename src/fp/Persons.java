package fp;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;


public class Persons {

    private List<Person> persons = List.of(
            new Person(1, "Alice", 22),
            new Person(2, "Bob", 20),
            new Person(3, "Carol", 21));

    @Test
    public void findsPersonById() {
        Person foundById = persons.stream()
                .filter(person -> person.getId() == 2)
                .toList().get(0);
        System.out.println(foundById);
    }

    @Test
    public void removePersonById() {
        List<Person> data = new ArrayList<>(persons);
        Person foundById = data.stream()
                .filter(person -> person.getId() == 2)
                .toList().get(0);
        data.remove(foundById);
        System.out.println(data);
    }

    @Test
    public void findsPersonNames() {
        String names = persons.stream()
                .map(person -> person.getName())
                .collect(Collectors.joining(", "));

        System.out.println(names);
    }

    @Test
    public void reverseSortedByAge() {
        List<Person> sortedByAge = persons.stream()
                .sorted((person, other) -> other.getAge().compareTo(person.getAge()))
                .toList();

        System.out.println(sortedByAge);
    }

}
