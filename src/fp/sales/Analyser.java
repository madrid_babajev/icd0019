package fp.sales;

import java.time.LocalDate;
import java.time.Month;
import java.time.MonthDay;
import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class Analyser {

    private Repository repository;

    public Analyser(Repository repository) {
        this.repository = repository;
    }

    public Double getTotalSales() {
        Double ret = repository.getEntries().stream()
                .mapToDouble(entry -> entry.getAmount())
                .sum();

        return ret;
    }

    public Double getSalesByCategory(String category) {
        List<Entry> data = repository.getEntries();
        Double ret = data.stream()
                .filter(entry -> entry.getCategory().equals(category))
                .mapToDouble(entry -> entry.getAmount())
                .sum();
        return ret;
    }

    public Double getSalesBetween(LocalDate start, LocalDate end) {
        List<Entry> data = repository.getEntries().stream()
                .filter(entry -> between(start, end, entry))
                .toList();

        Double sum = data.stream()
                .mapToDouble(entry -> entry.getAmount())
                .sum();

        return sum;
    }

    private boolean between(LocalDate start, LocalDate end , Entry entry) {
        boolean isBefore = entry.getDate().isBefore(end);
        boolean isAfter = entry.getDate().isAfter(start);

        return isBefore && isAfter;
    }

    public String mostExpensiveItems() {
        List<Entry> data = repository.getEntries();
        String retString = data.stream()
                .sorted((a, b) -> b.getAmount().compareTo(a.getAmount()))
                .limit(3)
                .map(item -> item.getProductId())
                .sorted()
                .collect(Collectors.joining(", "));

        return retString;
    }

    public String statesWithBiggestSales() {
        Map<String, Double> map = repository.getEntries().stream()
                .collect(Collectors.toMap(
                        each -> each.getState(),
                        each -> each.getAmount(),
                        (a, b) -> a + b));
        String retMap = map.entrySet().stream()
                .sorted((a, b) -> b.getValue().compareTo(a.getValue()))
                .limit(3)
                .map(item -> item.getKey())
                .collect(Collectors.joining(", "));

        return retMap;
    }

    @Override
    public String toString() {
        return repository.getEntries().toString();
    }
}
