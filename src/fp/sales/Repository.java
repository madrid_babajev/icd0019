package fp.sales;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

public class Repository {

    private static final String FILE_PATH = "src/fp/sales/sales-data.csv";

    private DateTimeFormatter formatter = DateTimeFormatter
            .ofPattern("dd.MM.yyyy");

    public List<Entry> getEntries() {

        // reads lines form the file and creates entry objects for each line.
            try {
                List<List<String>> data = Files.readAllLines(Paths.get(FILE_PATH))
                        .stream()
                        .filter(item -> !Objects.equals(item.split("\t")[0], "Order Date"))
                        .map(item -> Arrays.asList(item.split("\t")))
                        .toList();

                List<Entry> someList = data.stream().map(row -> {
                    Entry tempEntry = new Entry();

                    tempEntry.setDate(LocalDate.parse(row.get(0), formatter));
                    tempEntry.setState(row.get(1));
                    tempEntry.setProductId(row.get(2));
                    tempEntry.setCategory(row.get(3));

                    tempEntry.setAmount(getDouble(row.get(5)));

                    return tempEntry;
                }).toList();

                return someList;
            }
            catch (Exception e) {
                System.out.println(e);
                return List.of();
            }
    }

    private Double getDouble(String input) {
        String left, right;
        left = input.split(",")[0];
        right = "0." + input.split(",")[1];

        return Double.parseDouble(left) + Double.parseDouble(right);
    }


}
