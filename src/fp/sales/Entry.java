package fp.sales;

import java.time.LocalDate;

public class Entry {

    private String productId;
    private LocalDate date;
    private String state;
    private String category;
    private Double amount;

//    public Entry() {
//        this.productId = getProductId();
//        this.date = getDate();
//        this.state = getState();
//        this.category = getCategory();
//        this.amount = getAmount();
//
//    }
//
    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {

        return this.getProductId() /*
        Add this to to see amount ->  + " == " + this.amount */;
    }
}
