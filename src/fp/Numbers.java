package fp;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class Numbers {

    private List<Integer> numbers = Arrays.asList(1, 3, 4, 51, 24, 5);

    @Test
    public void findsOddNumbers() {

        List<Integer> oddNumbers = numbers.stream()
                .filter(num -> num % 2 == 1)
                .toList();

        System.out.println(oddNumbers);
    }

    @Test
    public void findsOddNumbersOver10() {
        List<Integer> oddOver10 = numbers.stream()
                .filter(num -> num % 2 == 1)
                .filter(num -> num > 10)
                .toList();
        System.out.println(oddOver10);
    }

    @Test
    public void findsSquaredOddNumbers() {
        List<Integer> oddSquares = numbers.stream()
                .filter(num -> num % 2 == 1)
                .map(num -> num * num)
                .toList();
        System.out.println(oddSquares);
    }

    @Test
    public void findsSumOfOddNumbers() {
        Integer sum = numbers.stream()
                .filter(num -> num % 2 == 1)
                .mapToInt(num -> num)
                .sum();
        System.out.println(sum);
    }

}
