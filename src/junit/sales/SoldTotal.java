package junit.sales;

public class SoldTotal {

    private String id;
    private Integer totalAmount;

    public SoldTotal(String id, Integer totalAmount) {
        this.id = id;
        this.totalAmount = totalAmount;
    }

    public String getId() {
        return id;
    }

    public Integer getTotalAmount() {
        return totalAmount;
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof SoldTotal)) {
            return false;
        }
        return this.getId().equals(((SoldTotal) other).getId())
                && this.getTotalAmount().equals(((SoldTotal) other).totalAmount);
    }

    @Override
    public String toString() {
        return getId() + " - " + getTotalAmount();
    }
}
