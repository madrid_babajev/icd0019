package junit.sales;


public class TopSalesFinder {

    SalesRecord[] sales = new SalesRecord[5];
    int indexTracker = 0;

    public void registerSale(SalesRecord record) {

        // store sales record for later analyses by findItemsSoldOver()
        try {
            sales[indexTracker] = record;

        } catch (IndexOutOfBoundsException e) {
            increaseSalesArray();
            sales[indexTracker] = record;
        }
        finally {
            indexTracker++;
        }
    }

    private void increaseSalesArray() {
        SalesRecord[] temp = new SalesRecord[sales.length * 2];

        for (int i = 0; i < sales.length; i++) {
            temp[i] = sales[i];
        }
        sales = temp;
    }

    public String[] findItemsSoldOver(int amount) {

        // find ids of records that sold over specified amount.
        String[] retArray = new String[uniqueCounter()];

        int i = 0;
        for (SoldTotal totalForEach : getSoldTotalArray()) {
            if (totalForEach.getTotalAmount() > amount) {
                retArray[i] = totalForEach.getId();
                i++;
            }
        }

        return clearFromNullMain(retArray);
    }

    private String[] clearFromNullMain(String[] array) {
        int newArrayLength = 0;
        for (String id : array) {
            if (id != null) {
                newArrayLength++;
            }
        }

        int i = 0;
        String[] retArray = new String[newArrayLength];
        for (String id : array) {
            if (id != null) {
                retArray[i] = id;
                i++;
            }
        }
        return retArray;
    }

    private SalesRecord[] clearFromNull(SalesRecord[] array) {
        int newArrayLength = 0;
        for (SalesRecord record : array) {
            if (record != null) {
                newArrayLength++;
            }
        }

        int i = 0;
        SalesRecord[] retArray = new SalesRecord[newArrayLength];
        for (SalesRecord record : array) {
            if (record != null) {
                retArray[i] = record;
                i++;
            }
        }
        return retArray;
    }

    private SoldTotal[] getSoldTotalArray() {

        int dataLength = uniqueCounter();
        SoldTotal[] data = new SoldTotal[dataLength];

        int i = 1;
        boolean newCycle = false;
        boolean firstCycle = true;
        SalesRecord[] copy = clearFromNull(sales);
        String lastId = copy[copy.length - 1].getProductId();
        int temp = 0;
        String previousId = "";
        String[] identificators = uniqueIdentificators();
        for (String id : identificators) {
            if (firstCycle) {
                previousId = id;
                firstCycle = false;
            }
            if (!previousId.equals(id)) {
                newCycle = true;
            }
            if (newCycle) {
                data[i] = new SoldTotal(previousId, temp);
                i++;
                temp = 0;
                newCycle = false;
            }
            for (SalesRecord record : sales) {
                if (record != null && record.getProductId().equals(id)) {
                    temp += record.getItemsSold() * record.getProductPrice();
                }
            }
            previousId = id;
        }
        data[0] = new SoldTotal(lastId, temp);

        return data;
    }

    private String[] uniqueIdentificators() {
        String[] retArray = new String[100];

        int i = 0;
        for (SalesRecord record : sales) {
            if (record != null && !checkDuplicates(retArray, record.getProductId())) {
                 retArray[i] = record.getProductId();
                 i++;
            }
        }

        return clearFromNullMain(retArray);
    }

    private boolean checkDuplicates(String[] array, String productId) {
        for (String id : array) {
            if (productId != null && productId.equals(id)) {
                return true;
            }
        }
        return false;
    }

    private int uniqueCounter() {
        int retValue = 0;
        String[] productIds = new String[100];
        int i = 0;
        for (SalesRecord record : sales) {
            if (record != null && !checkDuplicates(productIds, record.getProductId())) {
                productIds[i] = record.getProductId();
                i++;
                retValue++;
            }
        }
        return retValue;
    }

}


