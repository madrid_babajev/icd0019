package junit;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.IntStream;

public class Code {

    public static void main(String[] args) {
        System.out.println(isSpecial(0));
        System.out.println(mode("Aacvd  aaa"));
        System.out.println(removeDuplicates(new int[] {100, 0, 3, 100, 0, 4, 562, 4}));
    }

    public static boolean isSpecial(int candidate) {

        int leftover = candidate % 11;
        return leftover >= 0 && leftover <= 3;

    }

    public static int longestStreak(String inputString) {

        if (inputString.equals("")) {
            return 0;
        }
        int maxStreak = 1;
        int newStreak = 0;
        char temp = ';';
        boolean firstCycle = true;

        for (char chr: inputString.toCharArray()) {
            if (firstCycle) {
                temp = chr;
                firstCycle = false;
            }

            if (chr == temp) {
                newStreak++;
            }
            else {
                temp = chr;
                if (maxStreak < newStreak) {
                    maxStreak = newStreak;
                }
                newStreak = 1;
            }
        }
        if (newStreak > maxStreak) {
            return newStreak;
        }
        else {
            return maxStreak;
        }
    }

    public static Character mode(String inputString) {

        if (inputString == null || inputString.equals("")) {
            return null;
        }
        // Create map that keeps the oodesign.order of items added.
        Map<Character, Integer> someMap = new LinkedHashMap<>();

        for (char symbol: inputString.toCharArray()) {
            if (someMap.containsKey(symbol)) {
                int old = someMap.get(symbol);
                int newValue = old + 1;
                someMap.put(symbol, newValue);
            }
            else {
                someMap.put(symbol, 1);
            }
        }

        // Count max value.
        int maxValue = 0;

        for (Map.Entry<Character, Integer> item: someMap.entrySet()) {
            if (item.getValue() > maxValue) {
                maxValue = item.getValue();
            }
        }

        // return the character needed.
        for (Map.Entry<Character, Integer> item : someMap.entrySet()) {
            if (item.getValue() == maxValue) {
                return item.getKey();
            }
        }
        return null;
    }

    public static int getCharacterCount(String allCharacters, char targetCharacter) {
        int counter = 0;

        for (char currentChar: allCharacters.toCharArray()) {
            if (currentChar == targetCharacter) {
                counter++;
            }
        }

        return counter;
    }

    public static int[] removeDuplicates(int[] integers) {
        int[] tempArray = new int[integers.length];
        int index = 0;
        boolean firstNull = true;

        for (int currentNum : integers) {
            boolean contains = IntStream.of(tempArray).anyMatch(x -> x == currentNum);
            if (!contains) {
                tempArray[index] = currentNum;
                index++;
            }
            else if (currentNum == 0 && firstNull) {
                tempArray[index] = 0;
                index++;
                firstNull = false;
            }
        }

//
//        boolean flag = false;
//        int lengthCounter = 0;
//        for (int num: tempArray) {
//            if (flag && num == 0){
//                continue;
//            }
//
//            if (num == 0) {
//                flag = true;
//            }
//            else if (flag && num != 0) {
//                lengthCounter += 2;
//                flag = false;
//            }
//            else if (num != 0) {
//                lengthCounter++;
//            }
//        }

        index = 0;
        int[] retArray = new int[getLength(tempArray)];

        for (int num: tempArray) {
            try {
                retArray[index] = num;
                index++;
            }
            catch (ArrayIndexOutOfBoundsException e) {
                break;
            }
        }

        return retArray;
    }

    public static int getLength(int[] nums) {

        boolean flag = false;
        int lengthCounter = 0;
        for (int num: nums) {
            if (flag && num == 0){
                continue;
            }

            if (num == 0) {
                flag = true;
            }
            else if (flag && num != 0) {
                lengthCounter += 2;
                flag = false;
            }
            else if (num != 0) {
                lengthCounter++;
            }
        }

        return lengthCounter;
    }

    public static int sumIgnoringDuplicates(int[] integers) {
        int[] arrayGetter = removeDuplicates(integers);

        int accumulator = 0;

        for (int num: arrayGetter) {
            accumulator += num;
        }
        return accumulator;
    }

}
