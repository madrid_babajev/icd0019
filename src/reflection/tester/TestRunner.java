package reflection.tester;

import org.junit.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;

public class TestRunner {

    StringBuilder data = new StringBuilder();


    public void runTests(List<String> testClassNames) {
        for (String testClassName : testClassNames) {

            try {
                Class<?> clazz = Class.forName(testClassName);
                Method[] methods = clazz.getMethods();
                for (Method method : methods) {
                    MyTest myTest = method.getAnnotation(MyTest.class);

                    if (myTest == null) {
                        continue;
                    }
                    else {
                        Class<? extends Throwable> expected = myTest.expected();
                        Class<? extends Throwable> actual = MyTest.None.class;

                        Constructor<?> constr = clazz.getConstructor();

                        Object t = constr.newInstance();

                        try {
                            method.invoke(t);
                        } catch (InvocationTargetException e) {
                            actual = e.getCause().getClass();
                        }

                        appendToData(method, expected.isAssignableFrom(actual));
                    }
                }
            } catch (ClassNotFoundException | InvocationTargetException | NoSuchMethodException | InstantiationException | IllegalAccessException e) {

            }
        }
    }

    private void appendToData (Method method, boolean flag) {
        if (flag) {
            data.append(method.getName() + "() - OK");
        }
        else {
            data.append(method.getName() + "() - FAILED");
        }
        data.append(" ");
    }

    public String getResult() {
        return data.toString();
    }
}
