package reflection;

import reflection.serializer.SerializerTests;
import reflection.tester.TestRunnerTests;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        SerializerTests.class,
        TestRunnerTests.class})
public class TestSuite {

}