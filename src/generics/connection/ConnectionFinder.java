package generics.connection;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ConnectionFinder {

    private List<Connection> allConnections = new ArrayList<>();
    private List<Connection> trashBin = new ArrayList<>();
    private List<String> cantBeTo = new ArrayList<>();
    private List<List<String>> possiblePaths = new ArrayList<>();

    public void addAll(List<Connection> connections) {

        int i = 0;

        for (Connection connection : connections) {
            add(connection);
            System.out.println("Dobavil " + i);
            i++;
        }
    }

    public void add(Connection connection) {
        Connection reversal = new Connection(connection.getTo(), connection.getFrom());
        if (allConnections.contains(connection)
                || allConnections.contains(reversal)) {
            return;
        }
        allConnections.add(connection);
        allConnections.add(reversal);
    }

    public boolean hasConnection(String from, String to) {
        possiblePaths.clear();
        restoreData();
        cantBeTo.clear();

        List<String> nodes = getAllNodesFrom(from, from);
        while (true) {
            if (nodes.isEmpty()) {
                break;
            }
            if (nodes.contains(to)) {
                filterPossiblePaths();
                cantBeTo.clear();
                return true;
            }

            List<String> temp = new ArrayList<>();
            for (String root : nodes) {
                List<String> currentOptions = getAllNodesFrom(root, from);
                if (currentOptions.contains(to)) {
                    filterPossiblePaths();
                    cantBeTo.clear();
                    return true;
                }
                temp.addAll(currentOptions);
            }
            filterPossiblePaths();
            removeUselessConnections();
            nodes = temp;
        }
        filterPossiblePaths();
        cantBeTo.clear();
        return false;
    }

    private void removeUselessConnections() {
//        2-round roots: [z, y, c, d]
//        future banned roots -> z, y, c, d
//        if connections has a, b, g in it
//        -> terminate
        List<Connection> temp = new ArrayList<>();
        for (Connection connection : allConnections) {
            temp.add(connection);
            if (cantBeTo.contains(connection.getTo()) ||
                    cantBeTo.contains(connection.getFrom())) {
                temp.remove(connection);
                trashBin.add(connection);
            }
        }
        allConnections = temp;
        cantBeTo.clear();
    }

    private List<String> getAllNodesFrom(String root, String startRoute) {
        List<String> retList = new ArrayList<>();
        cantBeTo.add(root);

//      * root is getFrom(starting point) root : b <- connection(b, a)
//
//        Potential time possible?
//        boolean firstCaseFound = false;
//        int timer = 5;
        for (Connection connection : allConnections) {
            if (connection.getFrom().equals(root)) {
                if (cantBeTo.contains(connection.getTo())) {
                    continue;
                }
                if (root.equals(startRoute) &&
                        !possiblePaths.contains(Arrays.asList(root))) {
                    possiblePaths.add(Arrays.asList(root));
                }
                possiblePaths.add(findPossiblePaths(connection.getTo()));
                retList.add(connection.getTo());
            }
        }

        return retList;
    }

    private List<String> findPossiblePaths(String newOption) {
//        (a, b) : c, d
//        (a, b, c)
//        (a, b, c) ; remove (a, b)
//        * * *
//        (a, g) : z, y
//        no connection (g, z)
//        -> pass
        List<String> temp = new ArrayList<>();
        for (List<String> possiblePath : possiblePaths) {
            if (allConnections.contains(
                    new Connection(possiblePath.get(possiblePath.size() - 1), newOption))) {
                temp.addAll(possiblePath);
                temp.add(newOption);
                break;
            }
        }
        return temp;
    }

    private void filterPossiblePaths() {
        int newMinValue = possiblePaths.get(possiblePaths.size() - 1).size();
        List<List<String>> removeAfter = new ArrayList<>();
        for (List<String> path : possiblePaths) {
            if (path.size() < newMinValue) {
                removeAfter.add(path);
            }
        }
        possiblePaths.removeAll(removeAfter);
    }

    public List<String> findConnection(String from, String to) {
        boolean connectionCheck = hasConnection(from, to);
        if (!connectionCheck) {
            return List.of();
        }
        List<String> retList = new ArrayList<>();
        for (List<String> path : possiblePaths) {
            if (path.get(0).equals(from) &&
                    path.get(path.size() - 1).equals(to)) {
                retList = path;
                break;
            }
        }

        return retList;
    }

    private void restoreData() {
        allConnections.addAll(trashBin);
        trashBin.clear();
    }
}
