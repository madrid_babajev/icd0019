package generics.recursion;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Recursion {

    public List<String> getParts(Path path) {

        List<String> list = new ArrayList<>();
        Path runner = path;

        while (runner != null) {
            list.add(runner.getFileName().toString());
            runner = runner.getParent();
        }
        Collections.reverse(list);

        return list;
    }

    public List<String> getParts2(Path path) {

        if (path != null) {
            System.out.println(path.getFileName());
            getParts2(path.getParent());
        }
        return null;
    }

    public List<String> getParts3(Path path, List<String> parts) {
        List<String> retList = parts;
        if (path != null) {
            retList.add(path.getFileName().toString());
            getParts3(path.getParent(), parts);
        }
        Collections.reverse(retList);

        return retList;

    }

    public List<String> getParts4(Path path) {

        if (path == null) {
            return new ArrayList<>();
        }
        List<String> result = getParts4(path.getParent());
        result.add(path.getFileName().toString());

        return result;
    }
}
