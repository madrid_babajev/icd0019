package generics.cart;

// import java.util.HashMap;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class ShoppingCart<T extends CartItem> {

    public List<Double> discountTracker;
    private LinkedHashMap<T, Integer> cart;

    public ShoppingCart() {
        this.cart = new LinkedHashMap<>();
        this.discountTracker = new ArrayList<>();
    }

    public void add(T item) {

        if (cart.isEmpty()) {
            cart.put(item, 1);
        }
        else {
            for (Map.Entry<T, Integer> currentItem : cart.entrySet()) {
                if (item.getId().equals(currentItem.getKey().getId())) {
                    increaseQuantity(item.getId());
                } else {
                    cart.put(item, 1);
                }
            }
        }

    }

    public void removeById(String id) {

        for (Map.Entry<T, Integer> item: cart.entrySet()) {
            if (id.equals(item.getKey().getId())) {
                cart.remove(item.getKey());
                break;
            }
        }
    }

    public Double getTotal() {
        Double accumulator = 0.0;
        for (Map.Entry<T, Integer> item: cart.entrySet()) {
            accumulator += item.getValue() * item.getKey().getPrice();
        }
        if (discountTracker.isEmpty()) {
            return accumulator;
        }
        else {
            for (Double discount: discountTracker) {
                accumulator = accumulator * (1 - discount / 100);
            }

            return accumulator;
        }
    }

    public void increaseQuantity(String id) {
        for (Map.Entry<T, Integer> item: cart.entrySet()) {
            if (id.equals(item.getKey().getId())) {
                int updatedValue = item.getValue() + 1;
                cart.put(item.getKey(), updatedValue);
                break;
            }
        }
    }

    public void applyDiscountPercentage(Double discount) {
        discountTracker.add(discount);
    }

    public void removeLastDiscount() {
        discountTracker.remove(discountTracker.size() - 1);
    }

    public void addAll(List<T> items) {
        for (T item: items) {
            add(item);
        }
    }

    @Override
    public String toString() {
        StringBuffer retString = new StringBuffer();
        int counter = 0;
        for (Map.Entry<T, Integer> item: cart.entrySet()) {
            String temp = String.format("(%s, %s, %s)", item.getKey().
                    getId(), item.getKey().getPrice().toString(),
                    item.getValue().toString());
            counter++;
            if (cart.size() == 1) {
                retString.append(temp);
                break;
            }
            if (counter != cart.size()) {
                retString.append(temp);
                retString.append(", ");
            }
            else {
                retString.append(temp);
            }
        }

        return retString.toString();
    }
}
