package exceptions.price;

public class PriceCalculator {

    public double calculatePrice() {

        try {
            double basePrice = calculateBasePrice();

            if (basePrice == -1) {
                return -1D;
            }

            return basePrice * (1 + getVat());
        }
        catch (MissingConstantException e) {
            return -1;
        }
    }

    private double getVat() {
        return 0.2;
    }

    private double calculateBasePrice() {
        // some complex calculation that produces 100 as net cost
        @SuppressWarnings("PMD.PrematureDeclaration")
        double netCost = 100D;

        int profitConstant = readProfitConstant();

        if (profitConstant == -1) {
            return -1D;
            }

        return netCost + (0.1 * profitConstant * netCost);
    }

    private int readProfitConstant() {
        // read constant from file
        // return -1 if some error happens
        try {
            int constant = MyFileResource.read();

            if (constant == -1) {
                throw new MissingConstantException();
            }
            return constant;
        } catch (RuntimeException e) {
            throw new MissingConstantException();
        }




        // this simulates described behavior
    }

}
