package exceptions.validate;

import org.junit.Test;
import org.junit.Assert;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class MinimumElementTests {

    @Test
    public void findsMinimumFromArrayOfNumbers() {

        assertThat(MinimumElement.minimumElement(new int[] { 1, 3 }), is(1));

        assertThat(MinimumElement.minimumElement(new int[] { 1, 0 }), is(0));
    }

    @Test
    public void checksForExceptions() {

        try {
            MinimumElement.minimumElement(null);
            Assert.fail("You should have thrown an exception");
        }
        catch (IllegalArgumentException e) {
            assertThat(1, is(1));
        }


        try {
            MinimumElement.minimumElement(new int[] {});
            Assert.fail("You should have thrown an exception");
        }
        catch (IllegalArgumentException e) {
            assertThat(1, is(1));
        }
    }

}
