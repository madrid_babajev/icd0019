package exceptions.bad;

public class BadTryCatch {

    public boolean containsSingleLetters(String input) {

        int index = 0;

        try {
            if (input.length() == 1) {
                return true;
            }
            while (index < input.length()) {
                if (input.charAt(index) == input.charAt(index + 1)) {
                    throw new RuntimeException();
                }

                index++;
            }
        }
//        catch (IllegalArgumentException e) {
//            return false;
//        }

        catch (StringIndexOutOfBoundsException e) {
            return true;
        }
        catch (RuntimeException e) {
            return false;
        }

        return true;
    }


}
