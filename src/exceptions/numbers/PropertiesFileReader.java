package exceptions.numbers;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

public class PropertiesFileReader {
    private static final String PATH_TEMPLATE = "src/exceptions/numbers/numbers_%s.properties";

    public Properties readProperties(String lang) {
        Properties properties = new Properties();
        FileInputStream is = null;

        try {
            is = new FileInputStream(String.format(PATH_TEMPLATE, lang));

            InputStreamReader reader = new InputStreamReader(
                    is, StandardCharsets.UTF_8);

            properties.load(reader);
            if (!properties.containsKey(String.valueOf(1))) {
                throw new MissingTranslationException();
            }
        } catch (FileNotFoundException e) {
            throw new MissingLanguageFileException();
        } catch (IllegalArgumentException | IOException e){
            throw new BrokenLanguageFileException();
        } finally {
            close(is);
        }

        return properties;

    }

    private void close(FileInputStream is) {
        if (is == null) {
            return;
        }

        try {
            is.close();
        } catch (IOException ignore) {}
    }

}
