package exceptions.numbers;

import java.util.Properties;


public class NumberConverter {

    public Properties properties;

    public NumberConverter(String lang) {

        PropertiesFileReader reader = new PropertiesFileReader();

        properties = reader.readProperties(lang);
    }

    public String numberInWords(Integer number) {

        if (properties.containsKey(String.valueOf(number))) {
            return properties.getProperty(String.valueOf(number));
        }
        else {
            String numAsString = String.valueOf(number);
            int[] trueValues = getTrueValues(numAsString);
//            Escaping the NullPointerException
            int hundreds = trueValues[0];
            int tens = trueValues[1] * 10;
            int ones = trueValues[2];

            return getFinalString(hundreds, tens, ones);
        }
    }

    private int[] getTrueValues(String numAsString) {
        if (numAsString.length() == 3) {
            return new int[] {Character.getNumericValue(numAsString.charAt(0)),
                    Character.getNumericValue(numAsString.charAt(1)),
                    Character.getNumericValue(numAsString.charAt(2))};
        } else if (numAsString.length() == 2) {
            return new int[] {0,
                    Character.getNumericValue(numAsString.charAt(0)),
                    Character.getNumericValue(numAsString.charAt(1))};
        } else {
            return new int[] {0, 0, Character.getNumericValue(numAsString.charAt(0))};
        }
    }

    private String getFinalString(int hundreds, int tens, int ones) {
        if (hundreds == 0) {
            return getLastPart(tens, ones);
        }
        else {
            String lastPart = getLastPart(tens, ones);
            String hundredsLine = getHundredsString(hundreds, lastPart);
            if (lastPart.equals("")) {
                return hundredsLine;
            }
            return hundredsLine + lastPart;
        }
    }

    private String getLastPart(int tens, int ones) {
        if (tens == 0) {
            if (properties.getProperty(String.valueOf(0)).
                    equals(properties.getProperty(String.valueOf(ones)))) {
                return "";
            }
            return properties.getProperty(String.valueOf(ones));
        } else if (tens == 10) {
            if (ones != 0) {
                if (properties.containsKey(String.valueOf(tens + ones))) {
                    return properties.getProperty(String.valueOf(tens + ones));
                }
                return properties.getProperty(String.valueOf(ones))
                        + properties.getProperty("teen");
            }
            return properties.getProperty(String.valueOf(10));

        } else {
            String tensLine = getTensString(tens, ones);
            String onesLine = properties.getProperty(String.valueOf(ones));
            if (onesLine.equals(properties.getProperty(String.valueOf(0)))) {
                return tensLine;
            }
            return tensLine + onesLine;
        }
    }

    private String getHundredsString(int hundreds, String otherPart) {
        if (otherPart.equals("")) {
            return properties.getProperty(String.valueOf(hundreds))
                    + properties.getProperty("hundreds-before-delimiter")
                    + properties.getProperty("hundred");
        }

        return properties.getProperty(String.valueOf(hundreds))
                + properties.getProperty("hundreds-before-delimiter")
                + properties.getProperty("hundred")
                + properties.getProperty("hundreds-after-delimiter");
    }

    private String getTensString(int tens, int ones) {
        if (ones == 0) {
            if (properties.containsKey(String.valueOf(tens))) {
                return properties.getProperty(String.valueOf(tens));
            }
            return properties.getProperty(String.valueOf(tens / 10))
                    + properties.getProperty("tens-suffix");
        } else {
            if (properties.containsKey(String.valueOf(tens))) {
                return properties.getProperty(String.valueOf(tens))
                        + properties.getProperty("tens-after-delimiter");
            }
            return properties.getProperty(String.valueOf(tens / 10))
                    + properties.getProperty("tens-suffix")
                    + properties.getProperty("tens-after-delimiter");
        }

    }
}


