package exceptions.basic;

public class TryCatchSample {
    public String readDataFrom(Resource resource) {
//        throw new RuntimeException("not implemented yet");
        resource.open();
        try {
            return resource.read();
        }

        catch (RuntimeException e) {
            return "someDefaultValue";
        }

        finally {
            resource.close();
        }
    }
}
